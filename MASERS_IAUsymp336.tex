% iaus2esa.tex -- sample pages for Proceedings IAU Symposium document class
% (based on v1.0 cca2esam.tex)
% v1.04 released 17 May 2004 by TechBooks
%% small changes and additions made by KAvdH/IAU 4 June 2004
% Copyright (2004) International Astronomical Union

\NeedsTeXFormat{LaTeX2e}

\documentclass{iau}
\usepackage{graphicx}
\usepackage[colorlinks]{hyperref}

\newcommand{\masers}{\textsc{masers}}

\title[Statistical equilibrium calculations]
{MASERS: A Python package for statistical equilibrium calculations applied to masers}

\author[R van Rooyen \& DJ van der Walt]   %% give here short author list %%
{R van Rooyen$^1$ \and DJ van der Walt$^2$}

\affiliation{$^1$SKA South Africa, \\ 3rd Floor, The Park, Park Road, Pinelands, 7405, Western Cape,
South Africa \\ email: {\tt ruby@ska.ac.za} \\[\affilskip] $^2$Department of Space Physics, North-West
University, \\ Potchefstroom Campus, 11 Hoffman Street, Potchefstroom, 2531 \\email: {\tt
johan.vanderwalt@nwu.ac.za}}

\pubyear{2017}
\volume{336}
\setcounter{page}{1}
\jname{Astrophysical Masers: Unlocking the Mysteries of the Universe}
\editors{A.C. Editor, B.D. Editor \& C.E. Editor, eds.}

\begin{document}
\maketitle

\begin{abstract}
The study of astrophysical maser formation provides a useful probe of the chemical composition and physical conditions of the sources they are observed in. This exploration requires continuously solving the SE equations for the populations of the energy levels in search of conditions that will produce an inversion. After evaluation of available implementations applying the Escape Probability approximation, the
\masers{}
solver was developed to provide an efficient and robust matrix inversion calculation. 
This open source package is hosted at \url{https://bitbucket.org/ruby_van_rooyen/masers}.

\keywords{masers, molecules, numerical modelling, instabilities}
\end{abstract}

\firstsection
\section{Introduction}
In order to understand the pumping mechanisms of masers, suitable models must solve the rate equations for non-LTE statistical equilibrium (SE). A realistic model for a line-emitting system must contain a sufficiently large number of levels and take into account all processes describing population exchange
\cite[(Sobolev \& Gray 2012)]{SobolevGray2012}.
These SE equations must be solved simultaneously over all levels using robust iterative solvers.

The Escape Probability approximation simplifies the coupling between radiative transfer and the SE equations by pre-applying some assumption based on photon propagation through various mediums. In addition, it also uses adjustable variable selection to computationally approximate the physical environment, which allows easy manipulation of parameter space under investigation to inspect pumping mechanisms. The main disadvantage with the use of adjustable, empirical parameters, is that with a departure from their optimum values, very slow convergence and even divergence may occur.

The most efficient way of solving for a large number of level populations is to express the rate equations as a matrix of coefficients acting on a vector of populations. In this form, the level populations can be obtained by a number of standard numerical methods, with the only prerequisite for a successful solution being a reasonable initial guess. Matrix computation requires memory to store
big matrices for the rate equations of molecules with a large number of levels. Calculations of pseudo-inversion can lead to destructive numerical instabilities if the implementation does not properly represent the mathematical nature of the equations and variables. Plus, numerical procedures are strongly convergent if the starting solution is not too far from the final solution, but fails to converge in typical non-LTE conditions.

The
\masers{}
package is developed in Python, provides a reasonably fast, stable algorithm that deals with the solution method's inherent numerical sensitivities; allows different maser geometries for calculation; includes the contribution of interacting background radiation fields, as well as other sources of opacity such as line overlap.

\section{Non-equilibrium inversion}
Applying the escape probability to the rate equations, the level populations calculation can be expressed as presented in Equation 2.7.1 from
\cite[Elitzur (1992)]{Elitzur1992}.
\begin{equation}
\label{eq:escapeproprate}
\begin{array}{rcl}
\frac{dn_i}{dt} & = &
- \sum\limits_{j < i} \left\{A_{ij}\beta_{ij}\left[n_i + W\aleph_{ij}(n_i - n_j)\right]  +
C_{ij}\left[n_i - n_j\exp\left(\frac{-h\nu_{ij}}{kT}\right)\right]\right\} \\
& + &
\sum\limits_{j > i} \frac{g_j}{g_i}\left\{A_{ji}\beta_{ji}\left[n_j + W\aleph_{ji}(n_j - n_i)\right]  +
C_{ji}\left[n_j - n_i\exp\left(\frac{-h\nu_{ji}}{kT}\right)\right]\right\} \\
\end{array}
\end{equation}

As a parametric model Equation~\ref{eq:escapeproprate} capture all its information within the following parameters: the rate coefficients $A_{ij}$ and $C_{ji}$; the number density of level $i$, $N_i = g_in_i$, where $g_i$ is the statistical weight; the information related to the maser environment such as the dilution factor $W$ and $\aleph_{ij}$, the photon occupancy number at transition frequency $\nu_{ij}$; as well as, geometry and kinematics of the masing region in $\beta_{ij}$.

Expressed as a matrix equation it becomes
$$\bf{Q}\bf{x} = \bf{b}$$
where ${\bf{b}} = [0,\cdots, 0, 1]^T$, $x_i = \frac{n_i}{n_{mol}}$ is the normalised fractional population density, with $n_{mol}$ the total population density of the molecule and $\mathbf{Q} = \mathbf{R} + \mathbf{\widetilde{C}}$.

\[
\begin{array}{ccc}

\begin{array}{rcl}
R_{ij} & = & \left\{
\begin{array}{ll}
A_{ji}\beta_{ji}(1 + X_{ji}) & i<j \\
A_{ij}\beta_{ij}\left(\frac{g_i}{g_j}\right)X_{ij} & j>i \\
- \sum\limits_{i \neq j}R_{ji} & j=i \\
\end{array} \right . \\
\end{array}
&
\mbox{and}
&
\begin{array}{rcl}
\widetilde{C}_{ij} & = & \left\{
\begin{array}{ll}
C_{ji} & j>i \\
- \sum\limits_{i \neq j}C_{ij} & j=i \\
\end{array} \right . \\
\end{array}

\end{array}
\]
where $R$ is the radiative and $\widetilde{C}$ the collisional components, and $X = \aleph_{bb} + [\aleph(T_{\ell d}) + w_d\aleph(T_{xd})](1-e^{-\tau_d}) + w_{HII}\aleph(T_e)(1-e^{-\tau_{HII}})$ is the radiative contribution of the background.

{\underline{\it Numerical limitation and instabilities}}.
Iterative methods are sensitive to divergence and oscillation (multiple valid solutions) since the initial estimate for the next iteration is simply the solution of the previous iteration and the process is repeated until calculated solution satisfies some convergence criteria.

Badly chosen convergence criteria can also contribute to numerical instabilities. The natural choice of excitation temperature calculation as convergence criteria showed sensitivity to divergence, caused by catastrophic cancellation due to the difference in small numbers in the denominator of the excitation temperature calculation, $T_{ex}\propto[\ln(x_lg_u)-\ln(x_ug_l)]^{-1}$. More reliable convergence is obtained by directly comparing level populations with $x_n$ and $x_{n-1}$, the level populations calculated during the current and previous evaluation.

Oscillating behaviour is only pronounced at lower transition levels and the solutions were found to become more stable if the next iteration is given some ``memory'' of the previous solutions. This was done using a running average calculation, $x_n = c_1\times x_n + c_2\times x_{n-1}$ where $c_1\leq1$ and $c_2\leq1$ are some weighting coefficients with $c_1+c_2=1$. The larger the coefficient for the previous solution the ``longer'' the memory of the next solution. For 
\masers{}
this ``memory'' was found to be fairly large with $x_n = 0.05\times x_n + 0.95\times x_{n-1}$. It should be noted that a ``long memory'', $c_2 \rightarrow 1$, requires a more stringent convergence limit, $|x_n-x_{n-1}|/x_{n-1} < 10^{-7}$.

Lastly, all numerical implementations must carry some awareness of precision limits and computer number formats. To take extremely small rate coefficients into account a larger number representation and small number calculation libraries must be used. Such specialised libraries use more memory and computation takes longer. In order ensure consistent calculations, the
\masers{}
calculator limits A-coefficients to be $\geq 10^{-13}$. This limit can be imposed safely since the Einstein-A coefficients describe the transition probability per unit time that an atom currently in the upper level will go to the lower level, which is unlikely if the Einstein A coefficient is extremely small.

{\underline{\it Results}}.
The
\masers{}
software was used to investigate the pumping of the $H_2CO$ maser in G37.55+0.20, using the formaldehyde molecular data,
\cite[Wiesenfeld \& Faure (2013)]{WiesenfeldFaure2013},
and comparing the output with results published in
\cite[{Van der Walt} (2014)]{VanderWalt2014}.
Implementing the environments described in Section 3 of
\cite[{Van der Walt} (2014)]{VanderWalt2014},
\masers{}
successfully recreates stimulated inversions of the $1_{10}$--$1_{11}$ (4.8\,GHz) transition. Good agreement was obtained with the published inversion results for collisionally, as well as radiatively excited dust and free-free continuum emission.

{\underline{\it False positives}}.
A parametric model captures all its information within its parameters with no reference to actual data, making these methods very reliant on intelligent input by the user to accurately describe the physical environment. A proper understanding of the model inputs, implementation and assumptions are essential. Even then the user should always evaluate the outcome against expectation to guard against false positives.

This behaviour can be simulated for formaldehyde masers with the following environment parameters: Calculate the optical depth over the molecular column density range $N_{H2CO} = 10^{13} - 10^{18}\,cm^{-2}$ for total density $n_{H2} = 10^3$, $10^4$, $10^5\,cm^{-3}$ and kinetic temperature $T_{kin} =10$, $20$, $30\,K$. To investigate radiative pumping by thermal dust emission, apply a dust background radiation contribution of $50\,K$. If the dust temperature is applied as a flat temperature, $T_{bb} =50\,K$, the model will calculate the background continuum contribution for a black body emitter, which will result in inversions over the column density range. However, when applying it as a dust temperature, $T_{d} = 50\,K$ there will be no inversions, as expected, since the dust grey body continuum contribution model is used.

\section{Line overlap}
A detailed model of the escape probability representation of the rate equations with line overlap is given in
\cite[Elitzur \& Netzer (1985)]{ElitzurNetzer1985},
which can be generalised to obtain:
%
\begin{equation}
\label{eq:overlapescapeproprate}
\begin{array}{rcl}
\frac{dN_i^a}{dt} & = & -N_i^a \left[\sum\limits_{j<i}A_{ij}^a \hat{\beta_a}(1+X_{ij}^a) + \sum\limits_{j>i}A_{ji}^a \hat{\beta_a}(\frac{g_j}{g_i})X_{ji}^a\right] \\
& + & \left[\sum\limits_{j<i}N_j^a A_{ij}^a \hat{\beta_a}(\frac{g_i}{g_j})X_{ij}^a + \sum\limits_{j>i}N_j^aA_{ji}^a \hat{\beta_a}(1+X_{ji}^a)\right] \\
& + & N_i^a \left[\sum\limits_{j<i}A_{ij}^a f_a(1-\beta_{T})(x_a+X_{ij}^a) + \sum\limits_{j>i}A_{ji}^a f_a(1-\beta_{T})(\frac{g_j}{g_i})X_{ji}^a\right] \\
& - & \left[\sum\limits_{j<i}N_j^a A_{ij}^a f_a(1-\beta_{T})(\frac{g_i}{g_j})X_{ij}^a + \sum\limits_{j>i}N_j^aA_{ji}^a f_a(1-\beta_{T})(x_a+X_{ji}^a)\right] \\
& + & \sum\limits_{j<i}(1-\beta_{T})x_a\left(\sum\limits_{\alpha}^{\alpha\neq a}f_\alpha A_{ul}^\alpha N_u^\alpha\right)_{ij}  - \sum\limits_{j>i}(1-\beta_{T})x_a\left(\sum\limits_{\alpha}^{\alpha\neq a}f_\alpha A_{ul}^\alpha N_u^\alpha\right)_{ji}
\end{array}
\end{equation}
%
where $line\ a$ is the transition line being evaluated and $\hat{\beta_a}=\left[1-(1-f_a)(1-\beta_a)\right]$.

Note, only the radiative emission is affected by line overlap, thus Equation~\ref{eq:escapeproprate} is rewritten separating non-overlap and overlap, without explicitly including the collisional contributions in Equation~\ref{eq:overlapescapeproprate}.  As with Equation~\ref{eq:escapeproprate} this grouping can be represented with a matrix equation $\mathbf{(R+\widetilde{C})n=0}$, where the radiative matrix now has three components

\[
\mathbf{R} = \mathbf{R_a - R_T + O}\, ,
\]

\noindent with $\mathbf{R_a}$ the non-overlap radiation matrix of $line\ a$, $\mathbf{R_T}$ the overlap region of $line\ a$ and $\mathbf{O}$ all contribution of overlapping lines to $line\ a$ matrices as follows:

\[
\begin{array}{ccc}

\begin{array}{rcl}
R_{ij}^a & = & \left\{
\begin{array}{ll}
A_{ji}\hat{\beta_a}(1 + X_{ji}) & i<j \\
A_{ij}\hat{\beta_a}\left(\frac{g_i}{g_j}\right)X_{ij} & i>j \\
- \sum\limits_{i \neq j}R_{ji} & i=j \\
\end{array} \right . \\
\end{array}
&
\mbox{and}
&
\begin{array}{rcl}
R_{ij}^T & = & \left\{
\begin{array}{ll}
A_{ji}\hat{\beta_T}(x_a + X_{ji}) & i<j \\
A_{ij}\hat{\beta_T}\left(\frac{g_i}{g_j}\right)X_{ij} & i>j \\
- \sum\limits_{i \neq j}R_{ji} & i=j \\
\end{array} \right . \\
\end{array}

\end{array}
\]
with $\hat{\beta_T}=f_a(1-\beta_T)$.

\[
\begin{array}{ccc}

\begin{array}{rcl}
O_{ij} & = & \left\{
\begin{array}{ll}
-(1-\beta_T)x_a\mathbf{\alpha_{ji}} & i<j \\
(1-\beta_T)x_a\mathbf{\alpha_{ij}} & i>j \\
0  & i=j \\
\end{array} \right . \\
\end{array}
&
\mbox{and}
&
\alpha_{ij}[a_u]=\sum\limits_{\begin{array}{c}\alpha_{ul} \\ \alpha \neq a\end{array}} f_{\alpha_{ul}}A_{\alpha_{ul}} \\

\end{array}
\]
where $\mathbf{\alpha_{ij}}$ is a row vector with elements for the upper, $u$, and lower, $l$, indices of all lines overlapping $line\ a$.

\section{Summary}
Fundamental to the study of astrophysical masers is the calculation of level populations for various physical conditions under which a population inversion can occur. The well-known escape probability method provides a powerful numerical algorithm for solving these non-linear multi-level problems within reasonable computational time.

Given the ease of implementation of the escape probability method, it is the de-facto choice of many level population calculator codes available, such as RADEX
\cite[({Van der Tak} et al 2007)]{VanderTak2007}.
However, care should be taken since the conditions under which population inversion occurs often causes instabilities during the calculation of the inevitable matrix inversion, making the results unreliable.

For this reason the
\masers{}
Python package was developed to provide stable solutions for the investigation of maser pumping in particular and level population calculations in general. This paper described the development and implementation details of this open source package, as well as some preliminary results for formaldehyde maser pumping mechanisms.

\begin{thebibliography}{}

\bibitem[Sobolev \& Gray (2012)]{SobolevGray2012}
{{Sobolev}, A.~M. and {Gray}, M.~D.} 2012,
\textit{IAU Symposium}, 287, 13 

\bibitem[Elitzur (1992)]{Elitzur1992}
{{Elitzur}, M.}, 1992,
\textit{Kluwer Academic Publishers}, {0-7923-1217-1 PB}

\bibitem[Elitzur \& Netzer (1985)]{ElitzurNetzer1985}
{{Elitzur}, M. and {Netzer}, H.}, 1985,
\textit{ApJ}, 291, 464

\bibitem[Wiesenfeld \& Faure (2013)]{WiesenfeldFaure2013}
{{Wiesenfeld}, L. and {Faure}, A.}, 2013,
\textit{MNRAS}, 432, 2573

\bibitem[{Van der Tak} \etal\ (2007)]{VanderTak2007}
{{van der Tak}, F.~F.~S. and {Black}, J.~H. and {Sch{\"o}ier}, F.~L. and {Jansen}, D.~J. and {van Dishoeck}, E.~F.}, 2007,
\textit{A\&A}, 468, 627

\bibitem[{Van der Walt} \etal\ (2014)]{VanderWalt2014}
{{Van der Walt}, D.~J.}, 2014,
\textit{A\&A}, 562, A68

\end{thebibliography}

\end{document}
